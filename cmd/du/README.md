# du (CLI tool)

`du` prints detailed disk usage information for filesystems to stdout.

## Installation

Use the go tool to install `du`:
```
go get -u gitlab.com/bzub/du/cmd/du

```

## Usage

Arguments given to `du` should be files and/or directories on your filesystem. `du` will print a tree of information recursively, where the total size of the contents of a directory is calculated, along with the names and sizes of all the files/directories that said directory comprise of.

```
$ "${GOPATH}/bin/du" /home/bzub/projects/home-lab/images/terraform/provider
[
        {
                "name": "/home/bzub/projects/home-lab/images/terraform/provider",
                "size": 4904,
                "children": [
                        {
                                "name": "/home/bzub/projects/home-lab/images/terraform/provider/acme/Dockerfile",
                                "size": 522
                        },
                        {
                                "name": "/home/bzub/projects/home-lab/images/terraform/provider/acme/cloudbuild.yaml",
                                "size": 429
                        },
                        {
                                "name": "/home/bzub/projects/home-lab/images/terraform/provider/aws/Dockerfile",
                                "size": 530
                        },
                        {
                                "name": "/home/bzub/projects/home-lab/images/terraform/provider/aws/cloudbuild.yaml",
                                "size": 266
                        },
                        {
                                "name": "/home/bzub/projects/home-lab/images/terraform/provider/google/Dockerfile",
                                "size": 553
                        },
                        {
                                "name": "/home/bzub/projects/home-lab/images/terraform/provider/google-beta/Dockerfile",
                                "size": 441
                        },
                        {
                                "name": "/home/bzub/projects/home-lab/images/terraform/provider/kubernetes/Dockerfile",
                                "size": 472
                        },
                        {
                                "name": "/home/bzub/projects/home-lab/images/terraform/provider/null/Dockerfile",
                                "size": 540
                        },
                        {
                                "name": "/home/bzub/projects/home-lab/images/terraform/provider/random/Dockerfile",
                                "size": 444
                        },
                        {
                                "name": "/home/bzub/projects/home-lab/images/terraform/provider/tls/Dockerfile",
                                "size": 441
                        },
                        {
                                "name": "/home/bzub/projects/home-lab/images/terraform/provider/tls/cloudbuild.yaml",
                                "size": 266
                        }
                ]
        }
]
```

With no arguments, `du` prints information regarding the current working
directory.

```
$ pwd
/home/bzub/projects/du
$ "${GOPATH}/bin/du"
[
        {
                "name": "/home/bzub/projects/du",
                "size": 31736,
                "children": [
                        {
                                "name": "/home/bzub/projects/du/.git/COMMIT_EDITMSG",
                                "size": 15
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/HEAD",
                                "size": 23
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/config",
                                "size": 251
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/description",
                                "size": 73
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/hooks/applypatch-msg.sample",
                                "size": 478
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/hooks/commit-msg.sample",
                                "size": 896
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/hooks/fsmonitor-watchman.sample",
                                "size": 3327
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/hooks/post-update.sample",
                                "size": 189
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/hooks/pre-applypatch.sample",
                                "size": 424
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/hooks/pre-commit.sample",
                                "size": 1638
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/hooks/pre-push.sample",
                                "size": 1348
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/hooks/pre-rebase.sample",
                                "size": 4898
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/hooks/pre-receive.sample",
                                "size": 544
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/hooks/prepare-commit-msg.sample",
                                "size": 1492
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/hooks/update.sample",
                                "size": 3610
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/index",
                                "size": 539
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/info/exclude",
                                "size": 240
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/logs/HEAD",
                                "size": 157
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/logs/refs/heads/master",
                                "size": 157
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/logs/refs/remotes/origin/master",
                                "size": 139
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/objects/29/3026666206d2c755919f23ff59e944afeb0bb3",
                                "size": 52
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/objects/2d/13123d89e5777861f92eb442b4fedaf7dab677",
                                "size": 172
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/objects/69/2c15246f48d6d94d1a2a98207fcb9d64ebcd14",
                                "size": 57
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/objects/72/5e9d54040df978514fd2d6459041dd7ad5f9eb",
                                "size": 430
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/objects/80/acca92b096395e3187e2a6c033bae7b3e55ebb",
                                "size": 269
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/objects/95/d5908ca422adb780650bac1c2f03c407fc3b79",
                                "size": 578
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/objects/be/f8ab7ea21353c8fea18e3ea88dbe8a0b63f769",
                                "size": 125
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/objects/bf/ef41d99b0f99d3b84973a5df8f6eb04d26e414",
                                "size": 130
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/objects/d6/d93f209a30446d07d92f3c78d2f8460494d57d",
                                "size": 1021
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/objects/fd/c1a39614fc00ad7d67c1f09d18487f8ad8800d",
                                "size": 44
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/refs/heads/master",
                                "size": 41
                        },
                        {
                                "name": "/home/bzub/projects/du/.git/refs/remotes/origin/master",
                                "size": 41
                        },
                        {
                                "name": "/home/bzub/projects/du/README.md",
                                "size": 395
                        },
                        {
                                "name": "/home/bzub/projects/du/cmd/du/README.md",
                                "size": 3212
                        },
                        {
                                "name": "/home/bzub/projects/du/cmd/du/main.go",
                                "size": 1204
                        },
                        {
                                "name": "/home/bzub/projects/du/du.go",
                                "size": 816
                        },
                        {
                                "name": "/home/bzub/projects/du/filesystem/diskusage.go",
                                "size": 2191
                        },
                        {
                                "name": "/home/bzub/projects/du/go.mod",
                                "size": 140
                        },
                        {
                                "name": "/home/bzub/projects/du/go.sum",
                                "size": 380
                        }
                ]
        }
]
```
