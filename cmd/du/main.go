package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"gitlab.com/bzub/du"
	"gitlab.com/bzub/du/filesystem"
)

func main() {
	// paths may be directories and/or paths.
	paths := os.Args[1:]

	if len(paths) == 0 {
		// Use the current working directory if there are no arguments.
		cwd, err := os.Getwd()
		if err != nil {
			log.Printf("Unable to get current working directory: %s\n", err)
		}
		b, err := jsonReport(cwd)
		if err != nil {
			log.Printf("Unable to get disk usage: %s\n", err)
		}
		fmt.Println(string(b))
		return
	}

	b, err := jsonReport(paths...)
	if err != nil {
		log.Printf("Unable to get disk usage: %s\n", err)
	}
	fmt.Println(string(b))
}

// jsonReport prints a recursive disk usage report in json format to stdout.
func jsonReport(paths ...string) ([]byte, error) {
	duers := []du.DiskUsager{}

	for _, path := range paths {
		f, err := os.Open(path)
		if err != nil {
			log.Fatalf("Error opening \"%s\": %s\n", path, err)
		}
		duers = append(duers, &filesystem.File{File: f})
	}

	// Initialize DiskUsager(s)
	for _, duer := range duers {
		_, _, _, err := duer.DiskUsage()
		if err != nil {
			return nil, err
		}
	}

	return json.MarshalIndent(duers, "", "\t")
}
