package du

// A DiskUsager represents a resource that is able to report its disk
// usage as well as any child resources that comprise of said disk usage.
//
// See gitlab.com/bzub/du/filesystem for an example implementation that
// reports sizes of filesystem resources.
type DiskUsager interface {
	// DiskUsage should return a recursive totalBytes of a resource, as
	// well as the child resources.
	//
	// If children exists, the sum of their totalBytes should equal the
	// returned totalBytes. children should be nil if the underlying type
	// is incapable of containing sub-resources. For example, an empty
	// filesystem directory should return a zero length slice for
	// children, while a filesystem file should return nil.
	DiskUsage() (name string, totalBytes int64, children []DiskUsager, err error)
}
