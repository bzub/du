# du

Disk usage Go library and CLI tools.

## CLI Tools

One implementation of the `gitlab.com/bzub/du` and
`gitlab.com/bzub/du/filesystem` packages is available for now, simply called
`du`.

See the [README](https://gitlab.com/bzub/du/blob/master/cmd/du) for
installation and usage.

## Library

`du` consists of a simple library defining the `DiskUsager` interface, and an
implementation for filesystem files/directories.

For more information and testable examples please see:
- [du GoDoc](https://godoc.org/gitlab.com/bzub/du).
- [filesystem GoDoc](https://godoc.org/gitlab.com/bzub/du/filesystem)

### DiskUsager Interface

```
import "gitlab.com/bzub/du"
```

`du.DiskUsager` defines a simple interface that implementors can use as a
standardized way to depict recursive data size information. This could be
implemented to support S3 storage, for example.

### filesystem implementation

```
import "gitlab.com/bzub/du/filesystem"
```

`filesystem.File` is an example implementation of the `DiskUsager` interface
which acts on filesystem files local to your host.

### Testing

```
$ go test -v ./...
?       gitlab.com/bzub/du      [no test files]
?       gitlab.com/bzub/du/cmd/du       [no test files]
=== RUN   Example
--- PASS: Example (0.00s)
=== RUN   ExampleDiskUsage
--- PASS: ExampleDiskUsage (0.00s)
=== RUN   ExampleDiskUsage_symlinks
--- PASS: ExampleDiskUsage_symlinks (0.00s)
=== RUN   ExampleDiskUsage_empty
--- PASS: ExampleDiskUsage_empty (0.00s)
PASS
ok      gitlab.com/bzub/du/filesystem   0.011s
```
