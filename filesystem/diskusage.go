package filesystem

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/bzub/du"
)

// File wraps os.File and implements du.DiskUsager.
type File struct {
	*os.File
	children    []*File
	totalBytes  int64
	lastUpdated time.Time
}

// MarshalJSON implements json.Marshaler to generate custom json data.
func (f *File) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Name     string  `json:"name"`
		Size     int64   `json:"size"`
		Children []*File `json:"children,omitempty"`
	}{
		Name:     f.Name(),
		Size:     f.totalBytes,
		Children: f.children,
	})
}

// DiskUsage implements du.DiskUsager and returns a du.DiskUsager for size
// reporting. It does not follow symbolic links.
func (f *File) DiskUsage() (name string, totalBytes int64, children []du.DiskUsager, err error) {
	if f == nil || f.File == nil {
		return "", 0, nil, errors.New("File is nil.")
	}

	path, err := filepath.Abs(f.Name())
	if err != nil {
		return "", 0, nil, fmt.Errorf("Unable to find absolute path of \"%s\": %s\n", f.Name(), err)
	}
	f.lastUpdated = time.Now()

	err = filepath.Walk(path,
		func(subPath string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.IsDir() {
				return nil
			}

			// Special case: evaluating the root path itself
			if subPath == path {
				switch {
				case info.IsDir() && f.children == nil:
					f.children = []*File{}
				case !info.IsDir():
					// It's a file so we can use the file size
					f.totalBytes = info.Size()
				}
				return nil
			}

			subF, err := os.Open(subPath)
			if err != nil {
				return fmt.Errorf("Unable to open \"%s\": %s\n", subPath, err)
			}
			defer subF.Close()

			subDUer := &File{File: subF}
			_, size, _, err := subDUer.DiskUsage()
			if err != nil {
				return err
			}

			f.totalBytes = f.totalBytes + size
			f.children = append(f.children, subDUer)
			return nil
		},
	)
	if err != nil {
		return "", 0, nil, err
	}

	// Copy the children into a compatable slice of du.DiskUsager.
	// XXX: This might be poor design for performance.
	var duerSlice []du.DiskUsager = make([]du.DiskUsager, len(f.children))
	for i, v := range f.children {
		duerSlice[i] = v
	}

	return f.Name(), f.totalBytes, duerSlice, nil
}
