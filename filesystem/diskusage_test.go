package filesystem

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"testing"

	"gitlab.com/bzub/du"
)

var fs = &testFS{
	dirs: map[string]*testFS{
		"empty": &testFS{},
		"only-files": &testFS{
			files: []int64{1024, 2048, 3072},
		},
		"nested": &testFS{
			files: []int64{1024},
			dirs: map[string]*testFS{
				"2k": &testFS{
					files: []int64{2048},
					dirs: map[string]*testFS{
						"3k": &testFS{
							files: []int64{3072},
						},
					},
				},
			},
		},
		"symlinks": &testFS{
			symlinks: []string{"../only-files/1024", "../nested"},
		},
	},
}

var tmpDir string

type testFS struct {
	dirs     map[string]*testFS
	symlinks []string // string == target
	files    []int64  // filename == file size
}

func ExampleFile_DiskUsage() {
	f, err := os.Open(filepath.Clean(tmpDir + "/" + "nested"))
	if err != nil {
		log.Fatalf("Unable to open test fixture: %s\n", err)
	}

	duer := &File{File: f}
	if err := printDiskUsager(duer, ""); err != nil {
		log.Fatalf("Problem printing DiskUsager: %s", err)
	}

	// Output:
	// Name: nested
	// Size: 6144
	// -- Name: nested/1024
	// -- Size: 1024
	// -- Name: nested/2k/2048
	// -- Size: 2048
	// -- Name: nested/2k/3k/3072
	// -- Size: 3072
}

func ExampleFile_DiskUsage_onlyfiles() {
	f, err := os.Open(filepath.Clean(tmpDir + "/" + "only-files"))
	if err != nil {
		log.Fatalf("Unable to open test fixture: %s\n", err)
	}

	duer := &File{File: f}
	if err := printDiskUsager(duer, ""); err != nil {
		log.Fatalf("Problem printing DiskUsager: %s", err)
	}

	// Output:
	// Name: only-files
	// Size: 6144
	// -- Name: only-files/1024
	// -- Size: 1024
	// -- Name: only-files/2048
	// -- Size: 2048
	// -- Name: only-files/3072
	// -- Size: 3072
}

func ExampleFile_DiskUsage_symlinks() {
	f, err := os.Open(filepath.Clean(tmpDir + "/" + "symlinks"))
	if err != nil {
		log.Fatalf("Unable to open test fixture: %s\n", err)
	}

	duer := &File{File: f}
	if err := printDiskUsager(duer, ""); err != nil {
		log.Fatalf("Problem printing DiskUsager: %s", err)
	}

	// Output:
	// Name: symlinks
	// Size: 27
	// -- Name: symlinks/1024
	// -- Size: 18
	// -- Name: symlinks/nested
	// -- Size: 9
}

func ExampleFile_DiskUsage_empty() {
	f, err := os.Open(filepath.Clean(tmpDir + "/" + "empty"))
	if err != nil {
		log.Fatalf("Unable to open test fixture: %s\n", err)
	}

	duer := &File{File: f}
	if err := printDiskUsager(duer, ""); err != nil {
		log.Fatalf("Problem printing DiskUsager: %s", err)
	}

	// Output:
	// Name: empty
	// Size: 0
}

func TestMain(m *testing.M) {
	// Setup test fixtures
	td, err := ioutil.TempDir("", "du-fs-test")
	if err != nil {
		log.Fatalf("Problem creating temp dir: %s", err)
	}
	tmpDir = td
	if err := fs.mkFS(tmpDir); err != nil {
		log.Fatalf("Problem creating test fixtures: %s", err)
	}
	defer func() {
		// Cleanup test fixtures
		if err := os.RemoveAll(tmpDir); err != nil {
			log.Fatalf("Problem removing test fixures: %s", err)
		}
	}()

	result := m.Run()

	os.Exit(result)
}

func (tfs *testFS) mkFS(path string) error {
	for k, v := range tfs.dirs {
		subPath := filepath.Clean(path + "/" + k)
		mode := os.FileMode(int(0755))
		if err := os.Mkdir(subPath, os.ModeDir|mode); err != nil {
			return err
		}
		if err := v.mkFS(subPath); err != nil {
			return err
		}
	}
	for _, symlink := range tfs.symlinks {
		slPath := filepath.Clean(path + "/" + filepath.Base(symlink))
		if err := os.Symlink(symlink, slPath); err != nil {
			return err
		}
	}
	for _, fileSize := range tfs.files {
		filePath := filepath.Clean(path + "/" + strconv.FormatInt(fileSize, 10))
		f, err := os.Create(filePath)
		if err != nil {
			return err
		}
		defer f.Close()
		if err := f.Truncate(fileSize); err != nil {
			return err
		}
	}
	return nil
}

func printDiskUsager(duer du.DiskUsager, prefix string) error {
	name, totalBytes, children, err := duer.DiskUsage()
	if err != nil {
		log.Fatalf("DiskUsage: %s\n", err)
	}

	relPath, err := filepath.Rel(tmpDir, name)
	if err != nil {
		log.Fatalf("Problem getting relative path: %s", err)
	}

	fmt.Printf("%sName: %s\n", prefix, relPath)
	fmt.Printf("%sSize: %v\n", prefix, totalBytes)
	for _, child := range children {
		if err := printDiskUsager(child, prefix+"-- "); err != nil {
			return err
		}
	}
	return nil
}
